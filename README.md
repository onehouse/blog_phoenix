# Blog

This is a sample Elixir/Phoenix/Docker app.

## Docker

We're using `docker-compose` to break the web and database components of the dev
environment into Docker containers.  

To run the development server, you will need to run:

```bash
$ docker-compose up
```

You will still need to create and migrate the development database:

```bash
$ docker-compose run web mix do ecto.create, ecto.migrate
```

