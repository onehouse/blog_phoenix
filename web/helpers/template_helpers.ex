defmodule Blog.Helpers.TemplateHelpers do
  def active_nav_class(conn, controller) do
    if conn.private.phoenix_controller == controller do
      "active"
    else
      nil
    end
  end
end

