defmodule Blog.Admin.PostView do
  use Blog.Web, :view

  def format_datetime(ecto_datetime) do
    {:ok, datetime} = Ecto.DateTime.dump(ecto_datetime)
    datetime
    |> Timex.Date.from
    |> Timex.DateFormat.format!("%m/%d/%y %I:%M%P", :strftime)
  end
end
