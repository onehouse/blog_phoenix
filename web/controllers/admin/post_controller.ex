defmodule Blog.Admin.PostController do
  use Blog.Web, :controller

  alias Blog.{Repo, Post}

  plug :scrub_params, "post" when action in [:create, :update]

  def index(conn, _params) do
    posts = Repo.all(Post)
    render conn, "index.html", posts: posts
  end

  def new(conn, _pramas) do
    changeset = Post.changeset(%Post{})
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"post" => post_params}) do
    changeset = Post.changeset(%Post{}, post_params)
    case Repo.insert(changeset) do
      {:ok, _changeset} ->
        conn
        |> put_flash(:info, "Post created!")
        |> redirect(to: admin_post_path(conn, :index))
      {:error, changeset} ->
        render conn, "new.html", changeset: changeset
    end
  end

  def edit(conn, %{"id" => post_id}) do
    post = Repo.get(Post, post_id)
    changeset = Post.changeset(post)
    render conn, "edit.html", post: post, changeset: changeset
  end

  def update(conn, %{"id" => post_id, "post" => post_params}) do
    post = Repo.get(Post, post_id)
    changeset = Post.changeset(post, post_params)
    case Repo.update(changeset) do
      {:ok, _changeset} ->
        conn
        |> put_flash(:info, "Post updated!")
        |> redirect(to: admin_post_path(conn, :index))
      {:error, changeset} ->
        render conn, "edit.html", post: post, changeset: changeset
    end
  end

  def delete(conn, %{"id" => post_id}) do
    post = Repo.get(Post, post_id)
    Repo.delete!(post)
    conn
    |> put_flash(:info, "Post deleted!")
    |> redirect(to: admin_post_path(conn, :index))
  end
end
