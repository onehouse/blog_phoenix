defmodule Blog.Views.Admin.PostViewTest do
  use Blog.ConnCase, async: true 

  alias Blog.Admin.PostView

  test "formatting a datetime" do
    datetime = Ecto.DateTime.from_erl({{2015, 4, 20}, {10, 30, 0}})
    assert PostView.format_datetime(datetime) == "04/20/15 10:30am"
  end
end
