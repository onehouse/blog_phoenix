defmodule Blog.Helpers.TemplateHelpersTest do
  use ExUnit.Case

  alias Blog.Helpers.TemplateHelpers

  test "#active_nav_class for active nav" do
    conn = %{private: %{phoenix_controller: "foo"}}
    assert TemplateHelpers.active_nav_class(conn, "foo") == "active"
  end

  test "#active_nav_class for inactive nav" do
    conn = %{private: %{phoenix_controller: "bar"}}
    assert TemplateHelpers.active_nav_class(conn, "foo") == nil
  end
end
