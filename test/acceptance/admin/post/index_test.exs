Code.require_file("./test/acceptance/admin/post/helpers.exs") 

defmodule Blog.Acceptance.Admin.Post.IndexTest do
  use ExUnit.Case
  use Hound.Helpers
  import Blog.Acceptance.Admin.Post.Helpers

  hound_session

  setup do
    setup_with_inserted_post
  end

  test "the page shows the existing post", %{post: post} do
    navigate_to_posts_index_page

    page_title = find_element(:css, "h1#page-title")
    assert visible_text(page_title) == "Posts"

    post_tr = find_element(:id, "post-tr-#{post.id}")
    post_title = find_within_element(post_tr, :class, "post-title")
    assert visible_text(post_title) == post.title

    post_inserted_at = find_within_element(post_tr, :class, "post-inserted-at")
    assert visible_text(post_inserted_at) == "04/20/15 10:30am"
  end
end
