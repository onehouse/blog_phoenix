Code.require_file("./test/acceptance/admin/post/helpers.exs") 

defmodule Blog.Acceptance.Admin.Post.UpdateTest do
  use ExUnit.Case
  use Hound.Helpers
  import Blog.Acceptance.Admin.Post.Helpers

  hound_session

  setup do
    setup_with_inserted_post
  end

  defp navigate_to_edit(post) do
    navigate_to_posts_index_page

    post_tr = find_element(:id, "post-tr-#{post.id}")
    post_edit_link = find_within_element(post_tr, :class, "edit-link")
    click(post_edit_link)
  end

  test "updating with valid params", %{post: post} do
    navigate_to_edit(post)

    title_field = find_element(:id, "post_title")
    fill_field(title_field, "New Post Title")
    body_field = find_element(:id, "post_body")
    fill_field(body_field, "New post body.")
    submit_button = find_element(:id, "post-form-submit")
    submit_element(submit_button)

    flash_info_div = find_element(:class, "alert-info")
    assert visible_text(flash_info_div) == "Post updated!"
  end

  test "updating with invalid params", %{post: post} do
    navigate_to_edit(post)

    title_field = find_element(:id, "post_title")
    fill_field(title_field, "S")
    submit_button = find_element(:id, "post-form-submit")
    submit_element(submit_button)

    assert page_source =~ "Oops, something went wrong!"
  end
end
