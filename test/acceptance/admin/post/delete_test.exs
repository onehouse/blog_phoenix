Code.require_file("./test/acceptance/admin/post/helpers.exs") 

defmodule Blog.Acceptance.Admin.Post.DeletingTest do
  use ExUnit.Case
  use Hound.Helpers
  import Blog.Acceptance.Admin.Post.Helpers

  hound_session

  setup do
    setup_with_inserted_post
  end

  test "deleting a blog post", %{post: post} do
    navigate_to_posts_index_page

    assert length(find_all_elements(:class, "post-tr")) == 1

    post_tr = find_element(:id, "post-tr-#{post.id}")
    delete_link = find_within_element(post_tr, :class, "delete-link")
    click(delete_link)

    assert length(find_all_elements(:class, "post-tr")) == 0
    flash_div = find_element(:class, "alert-info")
    assert visible_text(flash_div) == "Post deleted!"
  end
end
