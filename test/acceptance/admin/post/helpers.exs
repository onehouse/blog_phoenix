defmodule Blog.Acceptance.Admin.Post.Helpers do
  use ExUnit.Case
  use Hound.Helpers
  alias Blog.{Repo, Post}

  def setup_with_inserted_post do
    Repo.delete_all(Post)
    post = Repo.insert!(%Post{
      title: "Foo", 
      body: "Bar",
      inserted_at: Ecto.DateTime.from_erl({{2015, 4, 20}, {10, 30, 0}}),
    })
    {:ok, post: post}
  end

  def navigate_to_posts_index_page do
    navigate_to("/")
    main_nav = find_element(:id, "main-nav")
    posts_link = find_within_element(main_nav, :id, "posts-link")
    click(posts_link)

    main_nav = find_element(:id, "main-nav")
    posts_link_container = find_within_element(
      main_nav, :id, "posts-link-container"
    )
    assert has_class?(posts_link_container, "active")
  end
end
