Code.require_file("./test/acceptance/admin/post/helpers.exs") 

defmodule Blog.Acceptance.Admin.Post.CreateTest do
  use ExUnit.Case
  use Hound.Helpers
  import Blog.Acceptance.Admin.Post.Helpers

  hound_session

  defp navigate_to_new_form do
    navigate_to_posts_index_page
    new_post_link = find_element(:id, "new-post-link")
    click(new_post_link)
  end

  test "creating a post with valid params" do
    navigate_to_new_form
    post_title_field = find_element(:id, "post_title")
    fill_field(post_title_field, "My Blog Post")
    post_body_field = find_element(:id, "post_body")
    fill_field(post_body_field, "Had lunch today.  It was great.")
    post_form_submit_button = find_element(:id, "post-form-submit")
    submit_element(post_form_submit_button)
    flash_info = find_element(:class, "alert-info")
    assert visible_text(flash_info) == "Post created!"
  end

  test "creating a post with invalid params" do
    navigate_to_new_form
    post_form_submit_button = find_element(:id, "post-form-submit")
    submit_element(post_form_submit_button)
    assert page_source =~ "Oops, something went wrong!"
  end
end
