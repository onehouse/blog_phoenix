defmodule Blog.Admin.PostControllerTest do
  use Blog.ConnCase

  alias Blog.{Repo, Post}

  @valid_attrs %{
    title: "Foo Title",
    body: "Bar Body",
  }

  def insert_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(Dict.merge(@valid_attrs, attrs))
    |> Repo.insert!
  end

  test "GET /admin/posts", %{conn: conn} do
    post = insert_post
    conn = get(conn, admin_post_path(conn, :index))
    assert html_response(conn, 200) =~ "Posts"
    assert String.contains?(conn.resp_body, post.title)
  end

  test "GET /admin/posts/new", %{conn: conn} do
    conn = get(conn, admin_post_path(conn, :new))
    assert html_response(conn, 200) =~ "New Post"
  end

  test "POST /admin/posts with valid params", %{conn: conn} do
    conn = post(conn, admin_post_path(conn, :create), post: @valid_attrs)
    assert redirected_to(conn) == admin_post_path(conn, :index)
    assert Repo.get_by(Post, @valid_attrs)
  end

  test "POST /admin/posts with invalid params", %{conn: conn} do
    conn = post(conn, admin_post_path(conn, :create), post: %{})
    assert html_response(conn, 200) =~ "New Post"
  end

  test "GET /admin/posts/:id/edit", %{conn: conn} do
    post = insert_post
    conn = get(conn, admin_post_path(conn, :edit, post))
    assert html_response(conn, 200) =~ "Edit Post"
  end

  test "PUT /admin/posts/:id with valid attributes", %{conn: conn} do
    post = insert_post
    updated_post_attrs = %{@valid_attrs | title: "Updated Title"}
    conn = put(conn, admin_post_path(conn, :update, post), post: updated_post_attrs)
    assert redirected_to(conn) == admin_post_path(conn, :index)
    assert Repo.get_by(Post, updated_post_attrs)
  end

  test "PUT /admin/posts/:id with invalid attributes", %{conn: conn} do
    post = insert_post
    updated_post_attrs = %{@valid_attrs | title: "S"}
    conn = put(conn, admin_post_path(conn, :update, post), post: updated_post_attrs)
    assert html_response(conn, 200) =~ "Edit Post"
  end

  test "DELETE /admin/posts/:id", %{conn: conn} do
    post_attrs = %{title: "post to delete"}
    post = insert_post(post_attrs)
    conn = delete(conn, admin_post_path(conn, :delete, post))
    assert redirected_to(conn) == admin_post_path(conn, :index)
    refute Repo.get_by(Post, post_attrs)
  end
end
