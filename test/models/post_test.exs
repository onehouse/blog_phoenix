defmodule Blog.PostTest do
  use Blog.ModelCase

  alias Blog.{Post}

  @valid_attrs %{
    title: "Foo",
    body: "Bar",
  }

  test "changeset with valid attrs" do
    changeset = Post.changeset(%Post{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attrs" do
    changeset = Post.changeset(%Post{}, %{})
    refute changeset.valid?
  end

  test "changeset validates title length" do
    changeset = Post.changeset(%Post{}, %{@valid_attrs | title: "S"})
    assert changeset.errors[:title] == 
      {"should be at least %{count} character(s)", [count: 3]}
  end
end
